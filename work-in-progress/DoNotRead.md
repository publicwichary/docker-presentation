## Task

-Przykład to hello world -> https://hub.docker.com/_/hello-world/
-wylistowanie z docker huba"docker search <nazwaObrazu>"
-pobranie obrazu "docker pull <nazwaObrazu>"
-wylistowanie pobranych obrazów "docker images"
-wylistowanie pracujących/zamkniętych kontenerów "docker ps -a"
-Official znaczy, że to oficjalny kontener licencjonowany, bezpieczny.
-Automated znaczy, że jest tak przygotowany że po każdym commicie będzie tworzył nowy obraz

UWAGA: komenda docker run <nazwaLubIdObrazu> zawsze tworzy nowe kontenery z obrazu, nie uruchamia ponownie zamkniętego kontenera!

obraz jest readonly
kontener jest stateless, czyli nawet jak mamy kontener z jakimś systemem czy bazą danych to jak go zamkniemy i uruchomimy ponownie to będzie czysty bez zmian

## Task
zobacz sobie obraz alpine (mega mały linux)

docker build -t <nazwaObrazu> . <- kropka wskazuje aktualny folder, bo tutaj trzeba przekazać kontekst czyli Dockerfile

## Task 04

- Push/Pull image
- Visit https://docs.docker.com/get-started/part2/