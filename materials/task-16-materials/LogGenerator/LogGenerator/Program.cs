﻿using System;
using System.Threading.Tasks;

namespace LogGenerator
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                Console.WriteLine($"--LOG-- {DateTime.Now:O} {Guid.NewGuid()}-{Guid.NewGuid()}-{Guid.NewGuid()}");
                Task.Delay(TimeSpan.FromSeconds(2)).Wait();
            }
        }
    }
}
