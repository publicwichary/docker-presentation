# Cheat sheet

command | description
---|---
docker version | verify instalation; print client version; print server version
docker pull `imageName` | download image
docker login | login user to registry
docker images | list all images
docker rmi `imageName` | remove image
docker tag `oldImageName` `newImageName` | rename image
docker run `imageName` | create container from image and start
docker ps | list running containers
docker ps -a | list all containers
docker rm `containerId` | remove stopped container
docker rm `containerId` -f | force remove container 
docker start `containerId` | start stopped container
docker logs `containerId` | print container logs
docker inspect `containerId` | print container details


![DockerArchitecture](./docker_arch.jpg)
