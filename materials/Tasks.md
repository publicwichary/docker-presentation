# Tasks

__All commands should be executed in PowerShell__

## Task 01

- Execute following command `docker version`
- Execute following command `docker info`
- Execute following command `docker pull hello-world`
- Execute following command `docker images`
- Execute following command `docker run hello-world`
- Execute following command `docker ps -a`
- Execute following command `docker rm containerId`

## Task 02

- Open hub.docker.com
- Search for Ubuntu
- Execute following command `docker pull ubuntu`
- Execute following command `docker run -it ubuntu`

## Task 03

- Visit `hub.docker.com`
- Docker hub account create (register profile)
- Open powershell and login `docker login` 

## Task 04

- Try to tag and re tag images
- See remove tag latest from image
- Try to run image for tag that does not exist
- Try to run image with new tag

## Task 05

- Run any image yourself
- Find image on `hub.docker.com`
- Pull any image from docker hub
- Run downloaded image yourself and verify if they work at your machine
- Nice images that can be easily tested: teamspeak, redis, mysql 

## Task 06 

- Push image to your own private registry
- Tag image in order to match registry requirements

## Task 07

- Run wordpress with database
- Please use mysql:5.7 database in container (IMPORTAINT please use 5.7 version)
- Use docker networks to connect containers
- mysql settings
```
MYSQL_DATABASE=exampledb
MYSQL_USER=exampleuser
MYSQL_PASSWORD=examplepass
MYSQL_RANDOM_ROOT_PASSWORD='1' 
```
- wordpress settings
```
WORDPRESS_DB_HOST=NAZWA_KONTENERA_BAZY_DANYCH
WORDPRESS_DB_USER=exampleuser
WORDPRESS_DB_PASSWORD=examplepass
WORDPRESS_DB_NAME=exampledb
```

## Task 08

- Run SQL Server in docker container. Follow https://hub.docker.com/_/microsoft-mssql-server
- Connect to container using SQL Server Management Studio (SSMS)
- Apply changes using SQL
- Stop and remove container
- Container should be started in background (optional)
- Container should restart during computer restart (optional) 
- Tip: Always ensure if a container has enough RAM in docker settings. It is especially important for memory consuming SQL Server container.

## Task 09
- Run SQL Server container that persist it’s state to volume
- Connect to container using SQL Server Management Studio
- Apply changes using SQL
- Stop and remove container
- Recreate container attached to the same volume
- Verify if data is present using SSMS
- Stop and remove container
https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-configure-docker?view=sql-server-2017

## Task 10

- Please open VS 2017 or VS 2019 and generate file structure for .net core web api project
- Please create Dockerfile and build image
- Run container from newly created image

## Task 11
uruchomienie nginx i hostowanie z niego plików
- materiały: index.html, main.css, nginx.conf, nginx.general.conf
- obraz powinien wystawiać port 80
- hostowane pliki muszą znaleźć się w folderze - /usr/share/nginx/html
- plik nginx.conf musi się znaleźć w /etc/nginx/conf.d/default.conf
- plik nginx.general.conf musi się znaleźć w /etc/nginx/customConfiguration/nginx.general.conf
- komenda do uruchomienia nginx -> nginx -g daemon off;

## Task 12
uruchomienie małego programu konsolowego .net w dokerze
- materiały: otrzymujesz kod aplikacji NoisyConsoleApp
- opublikowana dll z .net core musi wgrana w obraz
- podczas uruchamiania komendą docker run należy nadać dostęp do folderu poprzez parametr -v

## Task 13
zbudowanie samemu obrazu aplikacji (bez multistage ale z docker ignore)
- materiały: kod aplikacji WebApplication1
- najlepiej jako obraz bazowy należy użyć -> https://hub.docker.com/_/microsoft-dotnet-core-sdk
- aplikacja .net core musi zostać zbudowana komenda dotnet build
- aplikacja musi zostać zbudowana wewnątrz kontenera
- aplikacja .net core musi zostać opublikowana komenda dotnet publish
- aplikacja .net core jest uruchamiana przy pomocy dotnet nazwa.dll

## Task 14
docker compose
- materiały: kod aplikacji

## Task 15
uruchomienie aplikacji która generuje dane do volumenu, wykonanie backupu danych
- materiały: kod aplikacji DataManipulator, wraz z kodem dołączony jest Dockerfile którego należy użyć
- aplikacja po uruchomieniu generuje pliki tekstowe i zapisuje je do folderu /data 
- po skończeniu aplikacja kończy pracę (kontener przechodzi w stan exited)
- podczas uruchomienie aplikacji należy dołączyć volumen
- po wygenerowaniu warto podejrzeć czy pliki się dobrze wygenerowały przy pomocy osobnego kontenera
- `tar cvf /nazwaPlikuBackup.tar /folderKtórySieZbackupuje`
- tip: https://docs.docker.com/storage/volumes/#backup-restore-or-migrate-data-volumes

## Task 16

uruchomienie aplikacji i nasłuchiwanie na logi
 - materiały: kod aplikacji LogGenerator, wraz z kodem dołączony jest Dockerfile którego należy użyć
 - aplikacja LogGenerator po uruchomieniu w nieskończoność generuje logi do konsoli
 - należy kontener uruchomić w tle docker run -d 
 - należy podpiąć się i nasłuchiwać na logi std/out

## Task 17 

uruchomienie kontenera zwracającego twoje imię na porcie 5000
 - materiały brak
 - należy użyć gotowego obrazu https://hub.docker.com/r/hashicorp/http-echo

## Task 18

uruchomienie kontenera na klastrze K8S
 - materiały: należy sobie zainstalować następujące dodatki do VS Code "Kubernetes" oraz "Kubernetes Support"
 - materiały: kubeconfig file (każdy swój), IP klastra 139.59.158.93
 - kontener który ma być uruchomiony w ramach pod to ma być https://hub.docker.com/r/hashicorp/http-echo
 - podczas deployowania należy stworzyć plik z pod oraz service
