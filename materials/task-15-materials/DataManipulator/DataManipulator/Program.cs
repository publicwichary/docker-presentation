﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DataManipulator
{
    class Program
    {
        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            Task.Run(async () => { await MainAsync(); }).Wait();
        }

        private static async Task MainAsync()
        {
            var folderPath = "/data";
            if (Directory.Exists(folderPath))
            {
                Console.WriteLine("Folder data exist");
                for (var i = 0; i < 10; i++)
                {
                    using (var writer = File.CreateText(Path.Combine(folderPath, $"{Guid.NewGuid().ToString().Replace("-", string.Empty)}.txt")))
                    {
                        await writer.WriteLineAsync(GenerateData());
                    }
                }
            }
            else
            {
                Console.WriteLine("Directory not exist");
            }
        }

        private static string GenerateData()
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < 100; i++)
            {
                stringBuilder.AppendLine(Guid.NewGuid().ToString());
            }
            return stringBuilder.ToString();
        }
    }
}
