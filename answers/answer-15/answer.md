```
docker build -t answer:15 -f .\DataManipulator\Dockerfile .
```

```
docker run -d --name answer15 --mount 'type=volume,src=myvolume,dst=/data' answer:15
```

```
docker run -it --mount 'type=volume,src=myvolume,dst=/data' ubuntu /bin/bash
```

```
docker run --rm --volumes-from answer15 -v c:/Users/pwichary/Documents/TestFolder:/backup ubuntu tar cvf /backup/backup.tar /data
```