```
docker network create wordpress-network

docker run --name wordpress-mysql --network wordpress-network -d -e MYSQL_DATABASE=exampledb -e MYSQL_USER=exampleuser -e MYSQL_PASSWORD=examplepass -e MYSQL_RANDOM_ROOT_PASSWORD='1' mysql:5.7

docker run --name wordpress --network wordpress-network -d -p 8080:80 -e WORDPRESS_DB_HOST=wordpress-mysql -e WORDPRESS_DB_USER=exampleuser -e WORDPRESS_DB_PASSWORD=examplepass -e WORDPRESS_DB_NAME=exampledb wordpress
```