﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/sql
        [HttpGet("sql")]
        public async Task<ActionResult<string>> GetSql([FromServices] IConfiguration configuration)
        {
            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = configuration["DatabaseAddress"],
                UserID = "sa",
                Password = "8f^pD3=R#f&S2f9r",
                ConnectTimeout = 30,
                Encrypt = false,
                InitialCatalog = "MyDatabase",
                MultipleActiveResultSets = true,
                TrustServerCertificate = true
            };
            var sqlCommand = "SELECT * FROM People;";
            var result = new List<string>();
            using (var connection = new SqlConnection(sqlConnectionStringBuilder.ToString()))
            {
                await connection.OpenAsync();
                using (var command = new SqlCommand(sqlCommand, connection))
                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        result.Add($"{reader["Name"] } {reader["Age"] }");
                    }
                }
            }
            return Ok(result);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
