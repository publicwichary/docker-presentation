﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NoisyConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                args = new[] { "C:\\Users\\pwichary\\source\\repos\\docker-presentation\\answers\\answer-12\\SampleFolder" };
            }
            Task.Run(async () => { await MainAsync(args); }).Wait();
        }

        private static async Task MainAsync(string[] args)
        {
            var folderPath = args[0];
            var regex = new Regex("report(.*)\\.txt$");
            Console.WriteLine(folderPath);
            while (true)
            {
                if (Directory.Exists(args[0]))
                {
                    var filesPaths = Directory.GetFiles(folderPath, "*.txt").Where(x => !regex.IsMatch(x));
                    foreach (var filePath in filesPaths)
                    {
                        var text = await File.ReadAllTextAsync(filePath);
                        var reportFileName = $"report-{Guid.NewGuid().ToString().Substring(0, 6)}.txt";
                        var reportContent = $"report of content {text}";
                        await File.WriteAllTextAsync(Path.Combine(folderPath, reportFileName), reportContent, Encoding.UTF8);
                        File.Delete(filePath);
                    }
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }
                else
                {
                    Console.WriteLine("Directory not exist");
                }
            }
        }
    }
}
